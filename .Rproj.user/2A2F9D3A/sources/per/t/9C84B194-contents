MaidenFrame <- ResultFrame[grepl("Maiden",ResultFrame$RaceID,ignore.case = T) | grepl("mdn",ResultFrame$RaceID,ignore.case = T),]
# What <- MaidenFrame[MaidenFrame$Wins>0,]

MaidenFrame$RaceID <- droplevels(MaidenFrame$RaceID)
MaidenFrame$Weather <- factor(MaidenFrame$Weather)
MaidenFrame$HorseType <- factor(MaidenFrame$HorseType)
MaidenFrame$InvSP <- 1/MaidenFrame$SP
MaidenFrame$AproxOdds <- 1/(MaidenFrame$SP-1)
for(i in levels(MaidenFrame$RaceID)) {
  MaidenFrame$AproxOdds[MaidenFrame$RaceID %in% i] <- MaidenFrame$AproxOdds[MaidenFrame$RaceID %in% i]/sum(MaidenFrame$AproxOdds[MaidenFrame$RaceID %in% i])
}
MaidenFrame$RatingDays <- as.numeric(MaidenFrame$RatingDays)
MaidenFrame$LastDate <- as.numeric(MaidenFrame$LastDate)
MaidenFrame$AvgRest <- as.numeric(MaidenFrame$AvgRest)
MaidenFrame$Won <- MaidenFrame$Position == 1
MaidenFrame$Placed <- MaidenFrame$Position <= 3
MaidenFrame <- MaidenFrame[,!names(MaidenFrame)%in%c("LastRating","Avg3RatingChange","Rating","LastRatingChange","Position","RaceID","Date","HorseName","TrackName","Jockey","Placed")]

library(GGally)
# ggpairs(MaidenFrame[,c(1:10,30)])
# ggpairs(MaidenFrame[,c(11:20,30)])
# ggpairs(MaidenFrame[,c(21:30)])
# ggpairs(MaidenFrame)

set.seed(92746)
MaidenTraining <- sample(x = 1:length(MaidenFrame$Prize),size = round(length(MaidenFrame$Prize)*0.20))
MaidenTest <- MaidenFrame[MaidenTraining,]
MaidenTraining <- MaidenFrame[-MaidenTraining,]
rm(MaidenFrame)

library(glmmTMB)
library(DHARMa)

# BaseModel <- glm(formula = Won ~ SP,family = "binomial",data = MaidenTraining)
BaseModel <- glmmTMB(formula = Won ~ InvSP -1,data = MaidenTraining,family = binomial)
summary(BaseModel)

StartTime <- Sys.time()
for(k in 1:250){
  library(glmmTMB)
  # StartTime <- Sys.time()
  # Active <- rep(FALSE,times = 29)
  # ActiveVars <- c(29)
  Skip <- TRUE
  while(Skip){
    Skip <- tryCatch({
      ActiveVars <- sample(1:30,size = 5)
      BestVars <- ActiveVars
      BestVars_1 <- ActiveVars
      BestModel <- as.formula(paste("Won ~ ",paste(names(MaidenTraining)[BestVars],collapse = "+"),sep = ""))
      BestModel <- glmmTMB(formula = BestModel,data = MaidenTraining,family = binomial)
      FALSE
    },warning = function(cond){
      return(TRUE)
    },error = function(cond){
      return(TRUE)
    })
  }
  BestFit <- AIC(BestModel)
  MaxIter <- 50
  Iter <- 0
  
  while(Iter < MaxIter){
    if(length(BestVars) != 30) {
      ExitCondition <- TRUE
      for(i in (1:30)[-ActiveVars]) {
        Skip <- FALSE
        TempActiveVars <- c(ActiveVars,i)
        TempModel <- as.formula(paste("Won ~ ",paste(names(MaidenTraining)[TempActiveVars],collapse = "+"),sep = ""))
        TempModel <- tryCatch({
          glmmTMB(formula = TempModel,data = MaidenTraining,family = binomial)
        },error = function(cond){
          Skip <<- TRUE
          return(NA)
        },warning = function(cond){
          Skip <<- TRUE
          return(NA)
        })
        if(Skip) next
        TempPVals <- summary(TempModel)[["coefficients"]][["cond"]][-1,4]
        TempFit <- AIC(TempModel)
        if(TempFit < BestFit & any(TempPVals[grepl(names(MaidenTraining[i]),names(TempPVals))] < 0.1)) {
          BestVars <- TempActiveVars
          BestModel <- TempModel
          BestFit <- TempFit
          ExitCondition <- FALSE
        }
      }
      if(ExitCondition) break
    }
    PVals <- summary(BestModel)[["coefficients"]][["cond"]][-1,4]
    while(any(PVals > 0.1)){
      Skip <- FALSE
      if(any(sapply(MaidenTraining[,BestVars], class) == "factor")){
        NLevels <- rep(1,times = length(BestVars))
        MaidenFactors <- sapply(MaidenTraining[,BestVars], class) == "factor"
        NLevels[MaidenFactors] <- sapply(MaidenTraining[BestVars[MaidenFactors]], function(x) {length(levels(x))})-1
        CNLevels <- cumsum(NLevels)
        TempActiveVars <- which(CNLevels >= which.max(PVals))[1]
        # if(!is.factor(MaidenTraining[,BestVars[TempActiveVars]])) TempActiveVars <- TempActiveVars -1
        if(any(PVals[ifelse((TempActiveVars-1) < 1,1,CNLevels[(TempActiveVars-1)]+1):CNLevels[TempActiveVars]] < 0.1)) {
          PVals[ifelse((TempActiveVars-1) < 1,1,CNLevels[(TempActiveVars-1)]+1):CNLevels[TempActiveVars]] <- 0.01
          next
        }
        TempActiveVars <- BestVars[-TempActiveVars]
      } else {
        TempActiveVars <- BestVars[-which.max(PVals)]
      }
      TempModel <- as.formula(paste("Won ~ ",paste(names(MaidenTraining)[TempActiveVars],collapse = "+"),sep = ""))
      TempModel <- tryCatch({
        glmmTMB(formula = TempModel,data = MaidenTraining,family = binomial)
      },error = function(cond){
        Skip <<- TRUE
        return(NA)
      },warning = function(cond){
        Skip <<- TRUE
        return(NA)
      })
      if(Skip) {
        PVals[which.max(PVals)] <- 0.01
        next
      }
      BestVars <- TempActiveVars
      BestModel <- TempModel
      BestFit <- AIC(BestModel)
      PVals <- summary(BestModel)[["coefficients"]][["cond"]][-1,4]
    }
    ActiveVars <- BestVars
    Iter <- Iter+1
    print(Iter)
  }
  if(BestFit < BestFit2) {
    BestVars2 <- BestVars
    BestModel2 <- BestModel
    BestFit2 <- BestFit
  }
  # StopTime <- Sys.time()
  # TimeDelta <- StopTime - StartTime
}
StopTime <- Sys.time()
TimeDelta <- StopTime - StartTime

summary(BestModel2)

TestAIC <- function(Model,Data,Penalty = 2){
  YPred <- predict(Model,newdata = Data,type = "response")
  LogLikelihood <- sum(dbinom(Data$Won,1,YPred,TRUE))
  TestAIC <- -2*LogLikelihood + Penalty*length(summary(Model)[["coefficients"]][["cond"]][,1])
  return(TestAIC)
}

TestConfMatrix <- function(Model,Data,Threshold = 0.5){
  YPred <- predict(Model,newdata = Data,type = "response")
  YPred <- ifelse(YPred > Threshold,1,0)
  CM <- table(YPred,Data$Won)
  return(CM)
}

TestAIC(BestModel,MaidenTest)
TestAIC(BaseModel,MaidenTest)

TestConfMatrix(BestModel,MaidenTest,0.4)
TestConfMatrix(BaseModel,MaidenTest,0.4)


# TestPred <- predict(BestModel,newdata = MaidenTest,type = "response")
# TestPred <- ifelse(TestPred > 0.3,1,0)
# cm <- table(MaidenTest$Won,TestPred)
# cm
# BestVars1 <- BestVars
# BestModel1 <- BestModel
# BestFit1 <- AIC(BestModel1)
# BestVars1 <- c(27,10,8,21)
# BestModel1 <- as.formula(paste("Won ~ ",paste(names(MaidenTraining)[BestVars1],collapse = "+"),sep = ""))
# BestModel1 <- glmmTMB(formula = BestModel1,data = MaidenTraining,family = binomial)

FullModel <- glm(formula = Won ~ .,
                 family = "binomial",data = MaidenTraining)
FullModel <- glmmTMB(formula = as.formula(paste("Won ~ ",paste(names(MaidenTraining)[-34],sep = "+"),sep = "")),
                     data = MaidenTraining,family = binomial)
summary(FullModel)
# rm(MaidenTest,MaidenTraining,FullModel,BaseModel)
